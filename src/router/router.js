import createRouter from 'router5';
import browserPlugin from 'router5-plugin-browser';

export const pages = {
  LOAD_SCREEN: 'load_screen',
  ENTER_SCREEN: 'enter_screen',
  PROFILE: 'profile',
  TEST: 'test',
  WORD_PLAY: 'word_play',
};

const routes = [
  { name: pages.LOAD_SCREEN, path: '/' },
  { name: pages.ENTER_SCREEN, path: '/enter_screen' },
  { name: pages.PROFILE, path: '/profile' },
  { name: pages.TEST, path: '/test' },
  { name: pages.WORD_PLAY, path: '/word_play' },
];

export const initialize = () => {
  const router = createRouter(routes, { defaultRoute: pages.LOAD_SCREEN });

  router.usePlugin(browserPlugin());

  router.start();

  return router;
};