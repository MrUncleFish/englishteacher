export const APP_ID = 7515744;
export const API_URL = 'https://apidv.ru/api.php';
export const API_VERSION = '5.110';
export const AJAX_CONFIG = {
  headers: {
    "content-type": "application/json",
  }
};
