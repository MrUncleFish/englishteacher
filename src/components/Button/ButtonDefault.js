import React from 'react';
import PropTypes from "prop-types";

import "./ButtonDefault.css";
import classnames from "classnames";


const ButtonDefault = ({children, disabled = false, style, onClick}) => {

  const className = classnames('ButtonDefault', { [`ButtonDefault--disabled`]: disabled, [`ButtonDefault--active`]: !disabled });

  return (<div style={style} onClick={() => {if (!disabled && onClick) onClick()}} className={className}>{children}</div>)
};

ButtonDefault.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]).isRequired,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  style: PropTypes.object,
};

export default ButtonDefault;
